<h1 align="center">scrimba Airbnb Static Project</h1>

<div align="center">
   Solution for a challenge from  <a href="http://devchallenges.io" target="_blank">Devchallenges.io</a>.
</div>

<div align="center">
  <h3>
    <a href="https://scrimbastaticprojectrajashekarmanda.netlify.app/">
      Demo
    </a>
    <span> | </span>
    <a href="https://gitlab.com/rajashekarmanda3729/scrimbareactjsstaticproject-2.git">
      Solution
    </a>
    <span> | </span>
    <a href="https://www.figma.com/file/we3JWqp1tBW1iye778CFlb/Airbnb-Experiences-(Copy)?node-id=2-2&t=s5IhrpXc0EASvmIf-0">
      Challenge
    </a>
  </h3>
</div>

### UI Design 
![](src/assets/demoScreenshotApp.png)


### Setup
* if you want to start project with vite@reactJS use below command
*         npm create vite@latest
*         npm install
*         npm run dev
*         npm run build

the above commans for server run/install depedencies and build code.

### ReactJS Functionalities
* Used react-components & ReactDOM 
* Used CSS for styling
* Used third party packages

### Features
  * Having list of live events session's.
  * Complete details of event weather it's soldout or available & rating, location and price of event.
  * Added scrollbar to scroll each event see full details. 

### Contact

* Github [@rajashekarmanda](https://github.com/Rajashekarmanda)