import starImage from '../../assets/starImage.png'
import './style.css'

function Card(props) {
    const { title, reviewCount, openSpots, imageUrl, rating, location, price, status } = props
    const image = new URL(`../../assets/${imageUrl}`, import.meta.url)
    const statusClass = (status==='ONLINE') ? 'online-class' : ''
    const classIs = (statusClass == '') ? 'hidden' : ''
    return (
        <div className='card-container'>
            <div>
                <img src={image} alt='image' className='image' />
            </div>
            <div className='card-content-container'>
                <div className='card-content-image-container'>
                    <button type='button' className={`button ${statusClass}`}>{status}</button>
                    <img src={starImage} alt='starImage' className='star-image' />
                    <p className='rating'>{rating} </p>
                    <p className='position'>  ({reviewCount})  .</p>
                    <p className='country'> USA</p>
                </div>
                <div className='card-content-desc-container'>
                    <p className='title'>{title}</p>
                </div>
                <div className='card-content-desc-container'>
                    <p className='price'><span className='span'>From ${price}</span>{`/person`}</p>
                </div>
            </div>
        </div>
    )
}
export default Card