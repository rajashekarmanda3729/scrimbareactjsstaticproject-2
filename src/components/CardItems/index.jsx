import Card from '../Card'
import './style.css'
import usersData from '../../assets/usersData'

function CardItems() {
    return (
        <div className='cards-container'>
            {usersData.map(eachUser => {
                return <Card key={eachUser.id} title={eachUser.title} rating={eachUser.stats.rating} reviewCount={eachUser.stats.reviewCount}
                    price={eachUser.price} imageUrl={eachUser.coverImg} location={eachUser.location} openSpots={eachUser.openSpots} 
                    status={eachUser.status}/>
            })}
        </div>
    )
}
export default CardItems