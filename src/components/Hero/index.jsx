import groupImages from '../../assets/groupImages.png'

import './style.css'

function Hero() {
    return (
        <div className='hero-container'>
            <img src={groupImages} alt='groupImages' className='group-images' />
            <h1 className='hero-heading'>Online Experiences</h1>
            <p className='hero-description'>Join unique interactive activities led by one-of-a-kind hosts—all without leaving home.</p>
        </div>
    )
}
export default Hero