import airbnbLogo from '../../assets/airbnbLogo.png'
import './style.css'

function Navbar() {
    return (
        <nav className='navbar-container'>
            <img src={airbnbLogo} alt='logo' className='navbar-logo'/>
        </nav>
    )
}

export default Navbar