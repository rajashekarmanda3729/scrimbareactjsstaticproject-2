import { useState } from 'react'
import reactLogo from './assets/react.svg'
import viteLogo from '/vite.svg'
import './App.css'
import Navbar from './components/Navbar'
import Hero from './components/Hero'
import CardItems from './components/CardItems'

function App() {
  return (
    <div className="App">
      <Navbar />
      <Hero/>
      <CardItems />
    </div>
  )
}

export default App
